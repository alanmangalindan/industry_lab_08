package ictgradschool.industry.lab08.ex01;

import java.io.FileReader;
import java.io.IOException;

public class Ex1Q1 {

    private void fileReaderEx01() {
        int num = 0;
        FileReader fR = null;

        try {
            fR = new FileReader("input1.txt");
            num = fR.read();
            System.out.println(num);  // 87 (cast to char: W)
            System.out.println(fR.read());  // 105 (cast to char: i)
            System.out.println(fR.read());  // 108 (cast to char: l)
            System.out.println(fR.read());  // 108 (cast to char: l)
            fR.close();
        } catch (IOException e) {
            System.out.println("IO problem");
        }
    }

    public static void main(String[] args) {
        new Ex1Q1().fileReaderEx01();
    }
}
