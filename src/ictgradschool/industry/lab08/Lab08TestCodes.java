package ictgradschool.industry.lab08;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Lab08TestCodes {

    public void start() {

        File myFile = new File("numbersq15.txt");
        printSumOfFileContents(myFile);

    }

    public void printSumOfFileContents(File inputFile) {

        int sum = 0;

        try (BufferedReader bR = new BufferedReader(new FileReader(inputFile))) {

            String line;

            while ((line = bR.readLine()) != null) {
                int num = Integer.parseInt(line);
                sum += num;
            }
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
            return;
        }

        System.out.println("The total is: " + sum);
    }

    public void printBackwards(String s, int size) {
        if (size < 1) {
            return;
        }
        System.out.print(s.charAt(size - 1));
        printBackwards(s, size - 1);
    }

    public static void main(String[] args) {
        new Lab08TestCodes().start();
    }
}
