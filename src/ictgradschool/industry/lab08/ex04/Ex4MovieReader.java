package ictgradschool.industry.lab08.ex04;

import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieReader;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {

        // TODO Implement this with a Scanner
        File myFile = new File(fileName);
        Movie[] films = null;

        try (Scanner scanner = new Scanner(myFile)) {

            scanner.useDelimiter(",|\\r\\n"); // for MacOS, delimiter regex should be ",|\\n"

            films = new Movie[scanner.nextInt()];

            int i = 0;

            while (scanner.hasNext() && i < films.length) {
                films[i] = new Movie(scanner.next(), scanner.nextInt(), scanner.nextInt(), scanner.next());
                i++;
//                System.out.println(scanner.next());
//                System.out.println(scanner.nextInt());
//                System.out.println(scanner.nextInt());
//                System.out.println(scanner.next());
            }

        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
            System.exit(-1);
        }

        System.out.println("Movies loaded successfully from " + fileName + "!");
        return films;

    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}
