package ictgradschool.industry.lab08.ex01;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ExerciseOne {

    public void start() {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.

        FileReader fR = null;
        File myFile = new File("input2.txt");
        char[] charArray = new char[50]; // arbitrarily created a 50-element array
        int i = 0;

        try {
            fR = new FileReader(myFile);
            while (true) {
                int num = fR.read();
                if (num != -1) {
                    charArray[i] = (char) num;
                    if (charArray[i] == '\n') { // disregard new line character
                        continue;
                    }
                    if (charArray[i] == 'e' || charArray[i] == 'E') {
                        numE++;
                    }
                    i++;
                } else {
                    break;
                }
            }
            total = i;
            fR.close();
        } catch (IOException e) {
            System.out.println("IO problem");
        }

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.
        File myFile = new File("input2.txt");

        try (BufferedReader reader = new BufferedReader(new FileReader(myFile))) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                char[] charArray = line.toCharArray();
                total += line.length();
                for (char c: charArray) {
                    if (c == 'e' || c == 'E') {
                        numE++;
                    }
                }
            }
        } catch (IOException e) {
            System.out.println("IO problem");
        }

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
